# The sample conponent for declarative route filtering
Current Angular app demostrates a sample component `src/app/core/header/navigation/navigation.component.ts` which implements the logic of declarative route filtering. Such use case can be applicable when you want some of the main navigation routes being filtered based upon user's role or permissions. 

## Core logic

The `src/app/core/header/navigation/navigation-links.service.ts` service is the source definition of routes. Each route in this example can have nested routes, so you are able to flter out not only the top level routes but some of the nested ones as well. Also each route entry has the corresponding `roles` property which is an array of roles available for accessing it. In case the array is empty - entry is considered availbale for all user roles. Same logic goes for user `platforms`.

You can experiment with different combinations and see the changes on the UI by switching user configuration passed to `User` class model instance. For example, change the `role` property on line `21` inside `src/app/core/services/user/user.service.ts` to one of the passed ENUM values and see the changes immidiately on UI. The whole filtering logic is done using currying functions, see `src/app/core/header/navigation/navigation.service.ts`. 

The environment setup of current project is fully done with Docker so you don't need anything pre-installed on your OS to get up and running. Just follow the instructions below.

# Angular 7 app is using Core-Shared module structure + Docker

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.1.


## Requirements
Installed [Docker](https://www.docker.com/products/docker-desktop) (latest 18.x.x)

## Module Structure
Current Angular Starter project follows the Core-Shared module structure. `Core module` is ensured to be instantiated only once and so is a great place to hold singleton services which can be shared among other feature modules. `Shared module` is a place to keep all 'dumb' components (check-boxes, menus, widgets, etc). See the following instructions for more details - https://www.technouz.com/4772/angular-6-app-structure-with-multiple-modules/


## Dev Environment

- Build the docker image by running the following command: `docker build -t ng-app-chromium .`

- Install all yarn dependencies by running the following:
```
docker run -t --rm -v `pwd`:/usr/app -w /usr/app ng-app-chromium yarn
```
- Add npm/yarn dependencies only within docker container:
```
docker run -t --rm -v `pwd`:/usr/app -w /usr/app ng-app-chromium yarn add lodash
```

## Development server

- Run `docker-compose up`
- Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run the following to generate a new component:
```
docker run -t --rm -v `pwd`:/usr/app -w /usr/app ng-app-chromium ng generate component component-name
```
You can also use `... ng generate directive|pipe|service|class|guard|interface|enum|module`.


## Build

Run the following to build the project (The build artifacts will be stored in the `dist/` directory):
```
docker run -t --rm -v `pwd`:/usr/app -w /usr/app ng-app-chromium yarn build
```


## Running unit tests

Run the following to execute the unit tests via [Karma](https://karma-runner.github.io):
```
docker run -t --rm -v `pwd`:/usr/app -w /usr/app ng-app-chromium yarn test
```


## Running end-to-end tests

Run the following to execute the end-to-end tests via [Protractor](http://www.protractortest.org/):
```
docker run -t --rm -v `pwd`:/usr/app -w /usr/app ng-app-chromium yarn e2e
```
