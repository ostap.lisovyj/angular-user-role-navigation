import { USER_PLATFORMS, USER_ROLES } from '../constants/user.constants';

export interface IUser {
  firstname: string;
  lastname: string;
  platform: USER_PLATFORMS;
  role: USER_ROLES;
}

export class User implements IUser {
  public firstname: string;
  public lastname: string;
  public platform: USER_PLATFORMS;
  public role: USER_ROLES;

  constructor(data: IUser) {
    this.firstname = data.firstname;
    this.lastname = data.lastname;
    this.platform = data.platform;
    this.role = data.role;
  }
}
