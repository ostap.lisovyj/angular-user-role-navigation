export enum USER_ROLES {
  admin = 'admin',
  sales = 'sales',
  manager = 'manager'
}

export enum USER_PLATFORMS {
  customer = 'customer',
  employee = 'employee'
}
