import { Component, OnInit } from '@angular/core';
import { throwError, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { UserService } from '@app/core/services';
import { User } from '@app/shared/models';
import { NavigationService, NavigationLinksService } from './services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [NavigationService, NavigationLinksService]
})

export class HeaderComponent implements OnInit {
  public userDetails$: Observable<User>;

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.userDetails$ = this.userService.getUserDetails().pipe(
      tap((user: User) => {
        console.log(user);
      }),
      catchError((err: Error) => {
        return throwError(err);
      }));
  }

}
