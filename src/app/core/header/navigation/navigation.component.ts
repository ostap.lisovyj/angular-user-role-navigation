import { Component, OnInit, Input } from '@angular/core';
import { User } from '@app/shared/models';
import { MenuItem, NavigationService } from '../services';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  public navigationMenu$: Observable<MenuItem[]>;

  @Input() user: Observable<User>;

  constructor(private navigationService: NavigationService) { }

  ngOnInit() {
    this.navigationMenu$ = this.user.pipe(
      map((user: User) => {
        const menus = this.setNavLinksFromUserData(user);

        return menus;
      })
    );
  }

  private setNavLinksFromUserData(user: User): MenuItem[] {
    return this.navigationService.getNavigationMenu(user);
  }

}
