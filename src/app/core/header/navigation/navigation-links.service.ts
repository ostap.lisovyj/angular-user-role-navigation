import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { USER_ROLES } from '@app/shared/constants';

export interface MenuItem {
  title: string;
  routerLink?: string;
  externalLink?: string;
  roles: string[];
  platforms: number[];
  routes?: MenuItem[];
}

@Injectable()
export class NavigationLinksService {

  getNavigationLinks(): MenuItem[] {
    return [
      {
        title: 'Sales Apps',
        roles: [],
        platforms: [],
        routes: [
          {
            title: 'Demo',
            externalLink: `${location.origin}/#/demo`,
            roles: [USER_ROLES.admin],
            platforms: []
          },
          {
            title: 'Case Studies',
            externalLink: `${location.origin}/#/case_studies`,
            roles: [],
            platforms: []
          }
        ]
      },
      {
        title: 'Orders',
        roles: [
          USER_ROLES.admin,
          USER_ROLES.sales
        ],
        platforms: [],
        routes: [
          {
            title: 'Manage Orders',
            routerLink: '/orders',
            roles: [USER_ROLES.admin],
            platforms: []
          },
          {
            title: 'New Order',
            routerLink: '/order',
            roles: [USER_ROLES.admin],
            platforms: []
          },
          {
            title: 'Reports',
            routerLink: '/reports',
            roles: [],
            platforms: []
          },
          {
            title: 'Dashboard',
            routerLink: '/dashboard',
            roles: [],
            platforms: []
          }
        ]
      },
      {
        title: 'Settigns',
        roles: [
          USER_ROLES.admin,
          USER_ROLES.manager,
          USER_ROLES.sales
        ],
        platforms: [],
        routes: [
          {
            title: 'Manage Settings',
            routerLink: '/settings',
            roles: [],
            platforms: []
          },
        ]
      },
      {
        title: 'Admin',
        roles: [
          USER_ROLES.admin
        ],
        platforms: [],
        routes: [
          {
            title: 'Users',
            externalLink: `${location.origin}/#/users`, // old angular app
            roles: [],
            platforms: [],
          }
        ]
      },
    ];
  }

  constructor() {}
}
