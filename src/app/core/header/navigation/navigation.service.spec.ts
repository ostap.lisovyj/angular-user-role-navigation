import { TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { NavigationService } from './navigation.service';
import { NavigationLinksService } from './navigation-links.service';
import { MenuItem } from './navigation-links.service';
import { HeaderComponent } from '@app/core/header/header.component';
import { UserService } from '@app/core/services';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { User } from '@app/shared/models';
import { USER_ROLES, USER_PLATFORMS } from '@app/shared/constants/user.constants';

describe('HeaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [HeaderComponent],
      providers: [
        NavigationService,
        NavigationLinksService,
        UserService
      ]
    });
  });

  it('should be created', inject([NavigationService], (service: NavigationService) => {
    expect(service).toBeTruthy();
  }));

  it('should return and process navigation links for super user', inject([NavigationService],
    (service: NavigationService) => {
      const user = new User({
        firstname: 'test',
        lastname: 'test',
        role: USER_ROLES.admin,
        platform: USER_PLATFORMS.customer
      });

      const processedMenu = service.getNavigationMenu(user);

      expect(processedMenu.length).toEqual(4);

      const salesToolsMenu: MenuItem = Object.assign({}, processedMenu[0]);
      const salesLinks = salesToolsMenu.routes;
      expect(salesLinks).toBeDefined();
      expect(salesLinks.length).toEqual(2);
      expect(salesLinks[0].title).toContain('Demo');
      expect(salesLinks[1].title).toContain('Case Studies');
    }));
});
