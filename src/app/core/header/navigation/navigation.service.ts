import { Injectable } from '@angular/core';
import * as _ from 'lodash';

import { NavigationLinksService, MenuItem } from './navigation-links.service';
import { IUser } from '@app/shared/models';

@Injectable()
export class NavigationService {

  constructor(private navigationLinksService: NavigationLinksService) {}

  getNavigationMenu({ platform, role }: IUser): MenuItem[] {

    const navigationLinks = this.navigationLinksService.getNavigationLinks().slice(0);
    const isUserPlatformMatching = _.curry(this.isPropertyValueEmptyOrMatches)('platforms', platform);
    const isUserRoleMatching = _.curry(this.isPropertyValueEmptyOrMatches)('roles', role);

    // filtering menu items
    const filteredSubMenu: MenuItem[] = navigationLinks
      .filter(isUserPlatformMatching)
      .filter(isUserRoleMatching)
      .reduce((acc: MenuItem[], menuItem: MenuItem) => {
        const routes = menuItem.routes
        .filter(isUserPlatformMatching)
        .filter(isUserRoleMatching);

        acc.push(Object.assign(menuItem, {
          routes
        }));

        return acc;
      }, []);

    return filteredSubMenu;
  }

  isPropertyValueEmptyOrMatches(prop: string, compareToProp: string | number, menuItem: MenuItem) {
    return menuItem[prop].length === 0 || menuItem[prop].indexOf(compareToProp) > -1;
  }

}
