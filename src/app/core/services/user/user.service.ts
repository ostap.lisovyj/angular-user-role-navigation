import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User } from '@app/shared/models';
import { USER_ROLES, USER_PLATFORMS } from '@app/shared/constants/user.constants';


@Injectable()
export class UserService {
  private loggedInUser: Observable<User>;

  constructor() {}
  /**
   * Load user details and save observable in current service
   */
  getUserDetails(): Observable<User> {
    if (!this.loggedInUser) {
      this.loggedInUser = of(new User({
        firstname: 'Ostap',
        lastname: 'Lisovyi',
        role: USER_ROLES.sales,
        platform: USER_PLATFORMS.employee
      }));
    }

    return this.loggedInUser;
  }

}
