import { NgModule, SkipSelf, Optional } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CoreRoutingModule } from './core-routing.module';
import { UserService } from './services';
import { HeaderComponent } from './header/header.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './header/navigation/navigation.component';

@NgModule({
  imports: [
    NgbModule,
    CommonModule,
    HttpClientModule,
    CoreRoutingModule,
    NoopAnimationsModule
  ],
  declarations: [HeaderComponent, NavigationComponent],
  providers: [
    UserService
  ],
  exports: [CoreRoutingModule, HeaderComponent]
})
export class CoreModule {
  /* make sure CoreModule is imported only by one NgModule the AppModule */
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import only in AppModule');
    }
  }
}
