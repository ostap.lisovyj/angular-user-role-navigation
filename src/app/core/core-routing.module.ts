import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PATH } from '@app/shared/constants';

const routes: Routes = [
  {
    path: PATH.ROOT,
    redirectTo: PATH.DASHBOARD,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class CoreRoutingModule { }
