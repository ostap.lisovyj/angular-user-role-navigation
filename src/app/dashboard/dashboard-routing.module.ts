import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PATH } from '@app/shared/constants';
import { MainComponent } from './main/main.component';

const routes: Routes = [
  {
    path: PATH.ROOT,
    component: MainComponent,
    resolve: {}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
